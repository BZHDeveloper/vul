namespace Msys {
	public class Passwd {
		public string pw_name;
		public string pw_passwd;
		public ulong pw_uid;
		public ulong pw_gid;
		public string pw_gecos;
		public string pw_dir;
		public string pw_shell;
		
		internal Passwd() {}
	}
	
	static Gee.HashSet<Passwd> pwd_set;
	
	static void pwd_set_init() {
		if (pwd_set == null) {
			pwd_set = new Gee.HashSet<Passwd>();
			try {
				string output = "", err = "";
				Process.spawn_command_line_sync ("getent passwd", out output, out err);
				if (err.strip().length > 0)
					return;
				var dis = new DataInputStream (new MemoryInputStream.from_data (output.strip().data));
				string line = null;
				while ((line = dis.read_line()) != null) {
					var pwd = new Passwd();
					var parts = line.split (":");
					pwd.pw_name = parts[0];
					pwd.pw_passwd = parts[1];
					pwd.pw_uid = (ulong)int64.parse (parts[2]);
					pwd.pw_gid = (ulong)int64.parse (parts[3]);
					pwd.pw_gecos = parts[4];
					pwd.pw_dir = parts[5];
					pwd.pw_shell = parts[6];
					pwd_set.add (pwd);
				}
			}
			catch {}
		}
	}
	
	public Passwd? getpwnam (string name) {
		pwd_set_init();
		foreach (var pwd in pwd_set)
			if (pwd.pw_name == name)
				return pwd;
		return null;
	}
	
	public Passwd? getpwuid (int64 uid) {
		pwd_set_init();
		foreach (var pwd in pwd_set)
			if (pwd.pw_uid == uid)
				return pwd;
		return null;
	}
	
	public class Group {
		public string gr_name;
		public string gr_passwd;
		public ulong gr_gid;
		public string[] gr_mem;
		
		internal Group() {}
	}
	
	static Gee.HashSet<Group> grp_set;
	
	static void grp_set_init() {
		if (grp_set == null) {
			grp_set = new Gee.HashSet<Group>();
			try {
				string output = "", err = "";
				Process.spawn_command_line_sync ("getent group", out output, out err);
				if (err.strip().length > 0)
					return;
				var dis = new DataInputStream (new MemoryInputStream.from_data (output.strip().data));
				string line = null;
				while ((line = dis.read_line()) != null) {
					var grp = new Group();
					grp.gr_name = line.split (":")[0];
					grp.gr_passwd = line.split (":")[1];
					grp.gr_gid = (ulong)int64.parse (line.split (":")[2]);
					if (line.split (":").length > 3)
						grp.gr_mem = line.split (":")[3].split (",");
					grp_set.add (grp);
				}
			}
			catch {}
		}
	}
	
	public Group? getgrgid (int64 gid) {
		grp_set_init();
		foreach (var grp in grp_set)
			if (grp.gr_gid == gid)
				return grp;
		return null;
	}
	
	public Group? getgrnam (string name) {
		grp_set_init();
		foreach (var grp in grp_set)
			if (grp.gr_name == name)
				return grp;
		return null;
	}
}
