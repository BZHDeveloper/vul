namespace GText {

    public class String : Gee.AbstractCollection<unichar>, Transformable {
        Gee.ArrayList<unichar> list;

        construct {
            list = new Gee.ArrayList<unichar>();
        }

        public String (Value init = "") {
            append (init);
        }
        
        public void append (Value v) {
            if (v.type() == typeof (string)) {
                int i = 0;
                unichar u;
                var s = (string)v;
                while (s.get_next_char (ref i, out u))
                    list.add (u);
            }
            else if (v.type().is_a (typeof (Gee.Traversable))) {
                var t = (Gee.Traversable<unichar>)v;
                if (t.element_type != typeof (unichar))
                    return;
                t.foreach (u => list.add (u));
            }
            else {
                Value vu = Value (typeof (unichar));
                if (v.transform (ref vu))
                    list.add ((unichar)vu);
            }
        }

        public static String join (params Value?[] array) {
            var str = new String();
            foreach (var v in array)
                str.append (v);
            return str;
        }

        public String down() {
            var str = new String();
            str.add_all_iterator (list.map<unichar>(u => u.tolower ()));
            return str;
        }

        public String up() {
            var str = new String();
            str.add_all_iterator (list.map<unichar>(u => u.toupper ()));
            return str;
        }

        public Gee.List<String> split (Value delimiters) {
            var dlm = new String (delimiters);
            var sl = new Gee.ArrayList<String>();
            foreach (var p in to_string ().split (dlm.to_string ()))
                sl.add (new String (p));
            return sl;
        }

        public String reverse() {
            var str = new String();
            foreach (var u in list)
                str.insert (0, u);
            return str;
        }

        public String chomp() {
            var str = new String();
            str.add_all (list);
            while (str[str.size - 1].isspace () && str.size > 0)
                str.remove_at (str.size - 1);
            return str;
        }
        public String chug() {
            var str = new String();
            str.add_all (list);
            while (str[0].isspace () && str.size > 0)
                str.remove_at (0);
            return str;
        }

        public String strip() {
            return chomp().chug();
        }

        public bool has_prefix (Value v) {
            return to_string().has_prefix (new String (v).to_string ());
        }

        public bool has_suffix (Value v) {
            return to_string ().has_suffix (new String (v).to_string ());
        }

        public String replace (Value old, Value replacement) {
            var so = new String (old);
            var sr = new String (replacement);
            return new String(to_string().replace (so.to_string (), sr.to_string ()));
        }

        public new unichar get (int index) {
            return list[index];
        }

        public int index_of (unichar u) {
            return list.index_of (u);
        }

        public void insert (int index, unichar u) {
            list.insert (index, u);
        }

        public unichar remove_at (int index) {
            return list.remove_at (index);
        }

        public new void set (int index, unichar u) {
            list[index] = u;
        }

        public String slice (int start, int stop) {
            var sl = new String();
            var sub = list.slice (start, stop);
            if (sub != null)
                sl.add_all (sub);
            return sl;
        }

        public String substring (int start, int len = -1) {
            var length = (len == -1) ? list.size - start : len;
            if (length + start > list.size)
                length = list.size - start;
            return slice (start, start + length);
        }

        public void sort (CompareDataFunc<unichar>? func = null) {
            list.sort (func);
        }

        public override bool add (unichar u) {
            return list.add (u);
        }

        public override void clear() {
            list.clear ();
        }

        public override bool contains (unichar u) {
            return u in list;
        }

        public override Gee.Iterator<unichar> iterator() {
            return list.iterator ();
        }

        public override bool remove (unichar u) {
            return list.remove (u);
        }

        public bool transform (ref GLib.Value dest) {
            if (dest.type() != typeof (string))
                return false;
            dest = to_string();
            return true;
        }

        public string to_string() {
            var sl = new Gee.ArrayList<string>();
            sl.add_all_iterator (list.map<string>(u => u.to_string()));
            return string.joinv ("", sl.to_array ());
        }

        public override bool read_only {
            get {
                return false;
            }
        }

        public override int size {
            get {
                return list.size;
            }
        }
    }
}