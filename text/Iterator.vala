namespace GText {
    public interface Iterator : Object {
        public abstract Value? next_value();
    }
}