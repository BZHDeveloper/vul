namespace GText {
    public interface Iterable : Object {
        public abstract Iterator iterator();
    }
}