namespace GText {
    public interface Transformable : GLib.Object {
        public abstract bool transform (ref Value dest);

        public virtual T cast<T>() {
            return GText.cast<T> (this);
        }
    }
}