namespace GText {
    [CCode (has_target = false)]
    public delegate void TransformerFunc (Value a, ref Value b);

    internal class Transformer : Object {
        public Type source { get; construct; }

        public Type destination { get; construct; }

        public TransformerFunc func { get; construct; }

        public Transformer (Type src, Type dest, TransformerFunc f) {
            Object (source : src, destination : dest, func : f);
        }
    }

    static void str_to_string (Value va, ref Value vb) {
        vb = new String ((string)va);
    }

    static void bytes_to_bytearray (Value va, ref Value vb) {
        vb = new ByteArray.take (((Bytes)va).get_data ());
    }

    static void bytearray_to_bytes (Value va, ref Value vb) {
        var ba = (ByteArray)va;
        vb = new Bytes (ba.data);
    }

    static void string_to_regex (Value va, ref Value vb) {
        var str = (string)va;
        try {
            vb = new Regex (str);
        }
        catch {
            vb = new Regex("");
        }
    }

    static void regex_to_string (Value va, ref Value vb) {
        vb = ((Regex)va).get_pattern ();
    }

    static void datetime_to_string (Value va, ref Value vb) {
        vb = ((DateTime)va).to_string ();
    }

    static void string_to_datetime (Value va, ref Value vb) {
        var dt = new DateTime.from_iso8601 ((string)va, null);
        if (dt != null)
            vb = dt;
    }

    static void datetime_to_int (Value va, ref Value vb) {
        vb = ((DateTime)va).to_unix ();
    }

    static void int_to_datetime (Value va, ref Value vb) {
        var dt = new DateTime.from_unix_local ((int64)va);
        if (dt != null)
            vb = dt;
    }

    static Gee.ArrayList<Transformer> list;

    public static void register_transform_func (Type A, Type B, TransformerFunc func) {
        if (list == null)
            list = new Gee.ArrayList<Transformer>();
        list.add (new Transformer (A, B, func));
    }

    static bool transform_value (Value va, ref Value vb) {
        if (va.transform (ref vb))
            return true;
        foreach (var t in list) {
            if (t.destination == vb.type ()) {
                if (va.type ().is_a (t.source)) {
                    t.func (va, ref vb);
                    return true;
                }
                Value vc = Value (t.source);
                if (transform_value (va, ref vc)) {
                    t.func (vc, ref vb);
                    return true;
                }
            }
        }
        return false;
    }

    public static T cast<T> (GLib.Value val) {
        //Value.register_transform_func (GLib.Type src_type, GLib.Type dest_type, GLib.ValueTransform transform_func)
        Value.register_transform_func (typeof (string), typeof (String), str_to_string);

        Value dest = Value (typeof (T));
        if (val.type().is_a (typeof (Transformable))) {
            var t = (Transformable)val;
            if (!t.transform (ref dest))
                return 0;
        }
        else if (!transform_value (val, ref dest))
            return 0;
        if (!dest.type().is_a (typeof (T)))
            return 0;
        if (typeof(T).is_a (Type.ENUM))
            return (T)dest.get_enum ();
        if (typeof(T).is_a (Type.FLAGS))
            return (T)dest.get_flags ();
        if (typeof(T).is_a (Type.BOXED))
            return (T)dest.get_boxed();
        if (typeof(T).is_a (Type.OBJECT))
            return (T)dest.get_object();
        if (typeof(T) == typeof (bool))
            return (bool)dest;
        if (typeof(T) == typeof (string))
        return (string)dest;
        if (typeof(T) == typeof (double)) {
            double? ptr = (double)dest;
            return ptr;
        }
        if (typeof(T) == typeof (float)) {
            float? ptr = (float)dest;
            return ptr;
        }
        if (typeof(T) == typeof (int64)) {
            int64? ptr = (int64)dest;
            return ptr;
        }
        if (typeof(T) == typeof (uint64)) {
            uint64? ptr = (uint64)dest;
            return ptr;
        }
        if (typeof(T) == typeof (int))
            return (int)dest;
        if (typeof(T) == typeof (uint))
            return (uint)dest;
        if (typeof(T) == typeof (long))
            return (long)dest;
        if (typeof(T) == typeof (ulong))
            return (ulong)dest;
        if (typeof(T) == typeof (char))
            return (char)dest;
        if (typeof(T) == typeof (uint8))
            return (uint8)dest;
        return 0;
    }
}