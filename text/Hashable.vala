namespace GText {
    public interface Hashable : Object {
        public abstract uint hash();

        public abstract bool equals (Value value);
    }
}