namespace GJson.JsonSchema {
	public class SchemaString : Schema {
		public SchemaString() {
			GLib.Object (schema_type: SchemaType.STRING);
		}
		
		public int64 max_length { get; set; }
		public int64 min_length { get; set; } 
		
		public Regex pattern { get; set; }
	}
}
